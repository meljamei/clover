var User = require('../models/User.js');
var express = require('express');
var router = express.Router();

router.route('/')
    .get(function (req, res) {
        res.render('users', { title: 'Users', message: 'All Users' });
    })
    .post(function (req, res) {
        res.json('Created the User');
    });

router.route('/:id')
    .get(function (req, res, next) {
        res.render('userdetails', { title: `User ${req.params.id}`, message: `User ${req.params.id}` });
    })
    .put(function (req, res) {
        res.json('Updated the user');
    })
    .delete(function (req, res) {
        res.json('user Deleted');
    });

router.route('/me')
    .get(function (req, res, next) {
        res.render('profile', { title: `Profile`, message: `this is the User Page` });
    })
    .put(function (req, res) {
        res.json('Updated the user');
    })
    .delete(function (req, res) {
        res.json('user Deleted');
    })




module.exports = router;