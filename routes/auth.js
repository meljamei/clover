var User = require('../models/User.js');
var express = require('express');
var router = express.Router();

router.route('/signin')
    .get(function (req, res) {
        res.render('signin', { title: 'Sign in', message: 'Please Sign in' });
    })
    .post(function (req, res) {
        res.json('signed in');
    });

router.route('/signup')
    .get(function (req, res) {
        res.render('signup', { title: 'Sign up', message: 'Please Sign ip' });
    })
    .post(function (req, res) {
        res.json('signed Up');
    });

router.route('/forget')
    .get(function (req, res) {
        res.render('forget', { title: 'Forget Password?', message: 'Forget Password?' });
    })
    .post(function (req, res) {

    });

router.route('/logout')
    .get(function (req, res) {
        res.json('Logged out');
    })


module.exports = router;