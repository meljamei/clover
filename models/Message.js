var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MessageSchema = Schema({
    body: { type: String }
});

module.exports = mongoose.model('Message', MessageSchema);