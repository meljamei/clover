var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
    username:       { type: String, required: true, index: { unique: true } },
    password:       { type: String, required: true },
    language:       { type: String },
    age:            { type: String },
    interests:      [{ type: String }],
    occupation:     { type: String },
    location:       { type: String },
    education:      { type: String },
    nationality:    { type: String },
    lifeStyle:      { type: Number }
});

module.exports = mongoose.model('User', UserSchema);