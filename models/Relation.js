var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RelationSchema = Schema({
    body: { type: String }
});

module.exports = mongoose.model('Relation', RelationSchema);