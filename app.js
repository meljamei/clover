var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var bodyParser = require('body-parser');
var pug = require('pug');
var morgan = require('morgan');


var app = express();
var config = require('./config/config');

var indexRoute = require('./routes/index');
var authRoute = require('./routes/auth');
var userRoute = require('./routes/users');

app.use(morgan('dev'));
app.use(express.static(path.join(__dirname,'public')));
app.set('view engine', 'pug');
app.set('views', './views');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

// Db
mongoose.connect(config.db_Url);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Db is Connected');
});

// Routes
app.use('/', indexRoute);
app.use('/auth', authRoute);
app.use('/users', userRoute);


app.listen(3000, function(){
    console.log('App started on port 3000');
})